package stack;

public class StackItem {

	
	
	
	private Object item;
	private StackItem next;
	
	public StackItem (Object object) {
		this.item=object;
	}

	/**
	 * @return the next
	 */
	public StackItem getNext() {
		return next;
	}

	/**
	 * @param next the next to set
	 */
	public void setNext(StackItem next) {
		this.next = next;
	}

	public Object getItem() {
		return item;
	}
}
