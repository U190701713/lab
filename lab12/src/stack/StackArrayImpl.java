package stack;

import java.util.ArrayList;

public class StackArrayImpl implements Stack{

	ArrayList<Object> Stack =new ArrayList<Object>();
	@Override
	public void push(Object item) {
		Stack.add(0, item);
		
	}

	@Override
	public Object pop() {
		Object top=Stack.remove(0);
		return top;
	}

	@Override
	public boolean empty() {
		return Stack.size()==0;
	}

}
