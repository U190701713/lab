package demo;
import stack.*;
public class StackDemo {
	
public static void main(String[] args) {
	
	Stack stack = new StackArrayImpl();
	stack.push("A");
	stack.push(5);
	stack.push("z");
	stack.push("b");
	stack.push("Hello");

	while (!stack.empty()) {
		
		System.out.println(stack.pop());
		
		
	}
}

}
