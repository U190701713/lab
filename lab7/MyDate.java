
public class MyDate {
 private int day,month,year;
 //jan is 0
	public MyDate(int day, int month, int year) {
		// TODO Auto-generated constructor stub
		this.day=day;
		this.month=month-1;
		this.year=year;
	}
int[] maxdays= {31,29,31,30,31,30,31,31,30,31,30,31};
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public void incrementDay() {
		int maxday=maxdays[month];
		int newday=day+1;
		if (newday>maxday) {
			incrementMonth();
			day=1;
			
		}else if (month==1&&newday==29 &&(!leapyear())) {
			day=1;
			incrementMonth();
		}else {
			day=newday;
		}
		
		
		
	}

	private boolean leapyear() {
		
		
		return year%4==0?true:false;
	}

	public void incrementYear(int yeardiff) {
		year=year+yeardiff;
		if (month==1&&day==29 &&(!leapyear())) {
			day=28;
		}
		
	}

	public void decrementDay() {
		int newday=day-1;
		if (newday==0) {
			day=31;
			decrementMonth();
		}else {
			day=newday;
		}
		
	}

	public void decrementYear() {
		decrementYear(1);
		
	}

	public void decrementMonth() {
		incrementMonth(-1);
		
		
	}

	public void incrementDay(int diff) {
		while (diff>0) {
			incrementDay();
			diff--;
			
		}
		
	}

	public void decrementMonth(int month) {
		incrementMonth(-month);
		
	}

	public void decrementDay(int diff) {
		while (diff>0) {
			decrementDay();
			diff--;
		}
		
	}

	public void incrementMonth(int moonth) {
		int newmonth=(month+moonth)%12;
		int yeardiff=0;
		
		if (newmonth<0) {
			newmonth+=12;
			yeardiff=-1;
		}

		//System.out.println(yeardiff);
		yeardiff+=(month+moonth)/12;
		//System.out.println(yeardiff);
		this.month=newmonth;
		this.year+=yeardiff;
		
		if (day>maxdays[month]) {
			
			day=maxdays[month];
			
			if (month==1&&day==29 &&(!leapyear())) {
				day=28;
			}
		}
	}

	public void decrementYear(int year) {
		incrementYear(-year);
		
	}

	public void incrementMonth() {
		// TODO Auto-generated method stub
		incrementMonth(1);
	}

	public void incrementYear() {
		incrementYear(1);
		// TODO Auto-generated method stub
		
	}

	public boolean isBefore(MyDate anotherDate) {
		//check the year first
		if (this.year<anotherDate.year) {
			//System.out.println("year");
			return true;
		}
		//check the months
		else if (this.month<anotherDate.month) {
			//System.out.println("month");
			return true;
		}
		//check days
		else if (this.day<anotherDate.day) {
			//System.out.println("day");
			return true;
		}else {
			return false;
		}
	}

	public boolean isAfter(MyDate anotherDate) {
		//check the year first
		if (this.year>anotherDate.year) {
		//	System.out.println("year");
			return true;
		}
		//check the months
		else if (this.month>anotherDate.month) {
			//System.out.println("month");
			return true;
		}
		//check days
		else if (this.day>anotherDate.day) {
			//System.out.println("day");
			return true;
		}else {
			return false;
		}
	}

	public int dayDifference(MyDate anotherDate) {
		int counterofdays=0;
		while (this.day!=anotherDate.day) {
			counterofdays++;
			incrementDay();
		}
		return (counterofdays-1);
	}
public String toString() {
	return year+"-" +((month+1)<10?"0":"")+(month+1)+"-"+(day<10?"0":"")+day;
	
}
}
