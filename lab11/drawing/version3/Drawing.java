package drawing.version3;

import java.util.ArrayList;
import shapes.shape;

public class Drawing {
	
	private ArrayList<shape> shapes = new ArrayList<shape>();
	//private ArrayList<Rectangle> rectangles = new ArrayList<Rectangle>();
	
	public double calculateTotalArea(){
		double totalArea = 0;

		for (shape sh : shapes){  
			totalArea+=sh.area();
			     
		}			
		return totalArea;
	}
	
	public void addShape(shape shape) {
		shapes.add(shape);
	}
}
