package drawing.version2;

import java.util.ArrayList;

import shapes.Circle;
import shapes.Rectangle;
import shapes.Square;

public class Drawing {
	
	private ArrayList<Object> shapes= new ArrayList<Object>();
	//private ArrayList<Rectangle> rectangles = new ArrayList<Rectangle>();
	
	public double calculateTotalArea(){
		double totalArea = 0;

		
		
		for (Object shape : shapes){  
			if (shape instanceof Circle) {
				Circle MS = (Circle) shape;
				totalArea += MS.area();
			}else if (shape instanceof Rectangle) {
				Rectangle MS = (Rectangle) shape;
				totalArea += MS.area();
			} else if (shape instanceof Square) {
				Square MS = (Square) shape;
				totalArea += MS.area();
			}
			     
		}		
		return totalArea;
	}
	
	/*public void addCircle(Circle c) {
		circles.add(c);
	}
	*/
	public void addShape(Object shapeObject) {
		shapes.add(shapeObject);
	}
}
