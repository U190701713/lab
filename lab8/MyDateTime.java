import java.time.Year;



public class MyDateTime {
	MyDate date;
	MyTime time;
	

	public MyDateTime(MyDate date, MyTime time) {

		this.date=date;
		this.time=time;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public void incrementDay(int i) {
date.incrementDay(i);
		
	}

	public void incrementHour() {
		time.incrementhour();
		
	}

	public void incrementHour(int i) {
		// TODO Auto-generated method stub
		
		
		
		int daydiff=time.incrementhour(i);
		if (daydiff<0) {
			date.decrementDay(-daydiff);
		}else {
			
		
		date.incrementDay(daydiff);
	}}

	public void decrementHour(int i) {
		incrementHour(-i);
		
	}

	public void incrementMinute(int i) {
		int daydiff=time.incrementminute(i);
		if (daydiff<0) {
			date.decrementDay(-daydiff);
		}else {
			
		
		date.incrementDay(daydiff);
	}
	}

	public void decrementMinute(int i) {
		incrementMinute(-i);
		
	}

	public void incrementYear(int i) {
		date.incrementYear(i);
		
	}

	public void decrementDay() {
		date.decrementDay(1);;
		
	}

	public void decrementYear(int i) {
		date.decrementYear(i);
		
	}

	public void decrementMonth() {
date.decrementMonth();
		
	}

	public void decrementMonth(int i) {date.decrementMonth(i);
		
	}

	public void incrementDay() {
date.incrementDay();
		
	}

	public void decrementDay(int i) {
		date.decrementDay(i);
		
	}

	public void incrementMonth() {
		// TODO Auto-generated method stub
		incrementMonth(1);
		
	}

	public void incrementMonth(int i) {
		date.incrementMonth(i);
		
	}

	public void decrementYear() {
		date.decrementYear();
		
	}

	public void incrementYear() {
		incrementYear(1);
		
	}

	public boolean isBefore(MyDateTime anotherDateTime) {
		if (date.isBefore(anotherDateTime.date)) {
			return true;
		}else if(date.isAfter(anotherDateTime.date)){
			
		
		return false;
	}
	if (time.isBefore(anotherDateTime.time)) {
		return true;
	}	
	return false;
	
	
	
	
	
	}

	public boolean isAfter(MyDateTime anotherDateTime) {
		if (date.isAfter(anotherDateTime.date)) {
			return true;
		}else if(date.isBefore(anotherDateTime.date)){
			
		
		return false;
	}
	if (time.isAfter(anotherDateTime.time)) {
		return true;
	}	
	return false;	
	}

	public String dayTimeDifference(MyDateTime anotherDateTime) {
		int yy,mm,dd,hh,mimi;
		yy=this.date.year;
		mm=this.date.month;
		dd=this.date.day;
		hh=this.time.hour;
		mimi=this.time.minute;
		MyDate mDate=new MyDate(dd, mm+1, yy);
		MyTime mTime=new MyTime(hh, mimi);
		MyDateTime mdatetime=new MyDateTime(mDate, mTime);
		int ydiff,mdiff,ddiff;
		int hdiff,midiff;
		ydiff=0;
		mdiff=0;
		ddiff=0;
		hdiff=0;
		midiff=0;
		/*
		if (mdatetime.date.year!=anotherDateTime.date.year) {
			
			if (date.year>anotherDateTime.date.year) {
				while(date.year!=anotherDateTime.date.year)
				{mdatetime.date.decrementYear();
				ydiff++;
				}
			}
			else {
				
					while(mdatetime.date.year!=anotherDateTime.date.year)
					{mdatetime.date.incrementYear();
					ydiff++;
					}
				
			}
			
		}
if (mdatetime.date.month!=anotherDateTime.date.month) {
			
			if (mdatetime.date.month>anotherDateTime.date.month) {
				while(mdatetime.date.month!=anotherDateTime.date.month)
				{mdatetime.date.decrementMonth();
				mdiff++;
				}
			}
			else {
				
					while(mdatetime.date.month!=anotherDateTime.date.month)
					{mdatetime.date.incrementMonth();
					mdiff++;
					}
				
			}
			
		}
if (mdatetime.date.day!=anotherDateTime.date.day) {
	
	if (mdatetime.date.day>anotherDateTime.date.day) {
		while(mdatetime.date.day!=anotherDateTime.date.day)
		{mdatetime.date.decrementDay();
		ddiff++;
		}
	}
	else {
		
			while(mdatetime.date.day!=anotherDateTime.date.day)
			{mdatetime.date.incrementDay(1);
			ddiff++;
			}
		
	}
	
}
if (mdatetime.time.hour!=anotherDateTime.time.hour) {
	
	if (mdatetime.time.hour>anotherDateTime.time.hour) {
		while(mdatetime.time.hour!=anotherDateTime.time.hour)
		{mdatetime.time.incrementhour(-1);

		hdiff++;
		}
	}
	else {
		
			while(mdatetime.time.hour!=anotherDateTime.time.hour)
			{mdatetime.time.incrementhour(1);
			hdiff++;
			}
		
	}
	
}

if (mdatetime.time.minute!=anotherDateTime.time.minute) {
	
	if (mdatetime.time.minute>anotherDateTime.time.minute) {
		while(mdatetime.time.minute!=anotherDateTime.time.minute)
		{mdatetime.time.incrementminute(-1);

		hdiff++;
		}
	}
	else {
		
			while(mdatetime.time.minute!=anotherDateTime.time.minute)
			{mdatetime.time.incrementminute(1);
			hdiff++;
			}
		
	}
	
}*/
		//mdatetime.date!=anotherDateTime.date||mdatetime.time!=anotherDateTime.time
		
		/*date.decrementYear(anotherDateTime.date.year);
		date.decrementMonth(anotherDateTime.date.month);
		date.decrementDay(anotherDateTime.date.day);
		time.incrementhour(-anotherDateTime.time.hour);
		time.incrementminute(-anotherDateTime.time.minute);*/
		if (mdatetime.isAfter(anotherDateTime)) {
			while (mdatetime.date.year!=anotherDateTime.date.year||mdatetime.date.month!=anotherDateTime.date.month||mdatetime.date.day!=anotherDateTime.date.day||mdatetime.time.hour!=anotherDateTime.time.hour||mdatetime.time.minute!=anotherDateTime.time.minute) {
				mdatetime.time.incrementminute(-1);
				midiff++;
				if (midiff==60) {
					midiff=0;
					ddiff++;
				}

				if (hdiff==24) {
					hdiff=0;
					ddiff++;
				}
				if (mdatetime.date.maxDays[mdatetime.date.month]==ddiff) {
					ddiff=0;
					mdiff++;
				}
				if (mdiff==12) {
					ydiff++;
					mdiff=0;
				}
				
			}
		}else {
			while (mdatetime.time.hour!=anotherDateTime.time.hour||mdatetime.time.minute!=anotherDateTime.time.minute) {
				mdatetime.time.incrementminute(1);
				midiff++;
				if (midiff==60) {
					midiff=0;
					hdiff++;
				}
				if (hdiff==24) {
					hdiff=0;
					ddiff++;
					//mdatetime.incrementDay();
				}
				
			}
			while (mdatetime.date.year!=anotherDateTime.date.year||mdatetime.date.month!=anotherDateTime.date.month||mdatetime.date.day!=anotherDateTime.date.day) {
				mdatetime.incrementDay();
				ddiff++;
				if (mdatetime.date.maxDays[mdatetime.date.month-mdiff]==ddiff) {
					ddiff=0;
					mdiff++;
				}
				if (mdiff==12) {
					ydiff++;
					mdiff=0;
				}
		}}

		return (ydiff==0?"":ydiff+" " + "year(s) ")+(mdiff==0?"":mdiff+" " + "month(s) ")+(ddiff==0?"":--ddiff+" " + "day(s) ")+(hdiff==0?"":(hdiff)+" " + "hour(s) ")+(midiff==0?"":midiff+" " + "minute(s) ");
	}
	public String toString() {
		return date.toString()+ " "+ time.toString();
		}
	}

